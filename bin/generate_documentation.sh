#!/bin/bash

# Makes sure we're in the right current working directory
cd `dirname $0`

# Downloads phpDocumentor.phar if it doesn't exist yet
if [ ! -f phpDocumentor.phar ]; then
    wget http://phpdoc.org/phpDocumentor.phar
fi

# Generates the documentation
php phpDocumentor.phar --target="../docs/" --directory="../src/" --template="responsive-twig" --title="OAuth API Documentation" --defaultpackagename="OAuth" --encoding="UTF-8" --validate --force
