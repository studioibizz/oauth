<?php
/**
 * OAuth support for PHP
 *
 * Minimal required PHP version is 5.6
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @copyright  2017 Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @version    HG: $Id$
 * @link       https://developers.ibizz.nl/packages/oauth
 */
namespace StudioIbizz\OAuth;

/**
 * This class contains the OAuth v1 functionality
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @link       https://developers.ibizz.nl/packages/oauth
 */
class OAuth1 extends OAuth
{
    /**
     * Public key of the OAuth customer
     *
     * @access private
     * @var    string
     */
    private $oauth_token = null;
    
    /**
     * Secret key of the OAuth customer
     *
     * @access private
     * @var    string
     */
    private $oauth_token_secret = null;
    
    /**
     * The signature method
     *
     * @access private
     * @var    string
     */
    private $signature_method;
    
    /**
     * Public key of the OAuth application
     *
     * @access private
     * @var    string
     */
    private $consumer_key;
    
    /**
     * Secret key of the OAuth application
     *
     * @access private
     * @var    string
     */
    private $consumer_secret;

    /**
     * The OAuth version of this service (always 1)
     *
     * @access protected
     * @var    integer
     */
    protected static $oauth_version = 1;
    
    /**
     * Service provider URL for OAuth Echo
     *
     * @access private
     * @var    string
     */
    private $serviceprovider_url = null;
    
    /**
     * When OAuth Echo is enabled, this value is set to true
     *
     * @access private
     * @var    boolean
     */
    private $oauth_echo_enabled = false;
    
    /**
     * Name of the OAuth realm
     *
     * @access private
     * @var    string
     */
    private $oauth_realm = 'OAuth realm';
    
    /**
     * Creates a new OAuth object
     *
     * @param string $consumer_key     Public key of the OAuth application
     * @param string $consumer_secret  Secret key of the OAuth application
     * @param string $signature_method The signature method; currently supporting HMAC-SHA1 and PLAINTEXT
     * @param string $oauth_realm      The authentication realm
     *
     * @access public
     * @return null
     */
    public function __construct(
        $consumer_key,
        $consumer_secret,
        $signature_method = 'HMAC-SHA1',
        $oauth_realm = 'OAuth realm'
    ) {
        $this->consumer_key = trim($consumer_key);
        $this->consumer_secret = trim($consumer_secret);
        $this->signature_method = strtoupper(str_replace('_', '-', $signature_method));
        $this->oauth_realm = $oauth_realm;
        // Validates the input
        if (!in_array($this->signature_method, array('PLAINTEXT', 'HMAC-SHA1'))) {
            throw new OAuthException(
                'Signature method not supported: '.$this->signature_method,
                OAuthException::ERROR_SIGNATURE_METHOD_UNSUPPORTED
            );
        } elseif ($this->consumer_key == '') {
            throw new OAuthException('Consumer key is not specified', OAuthException::ERROR_NO_KEY);
        } elseif ($this->consumer_secret == '') {
            throw new OAuthException('Consumer secret is not specified', OAuthException::ERROR_NO_SECRET);
        }
        // By default, we'll verify the SSL certificate (if using HTTPS)
        $this->setCurlOption(CURLOPT_SSL_VERIFYHOST, 2);
        $this->setCurlOption(CURLOPT_SSL_VERIFYPEER, true);
    }
    
    /**
     * Sets the clients credentials
     *
     * @param string $oauth_token        The OAuth token
     * @param string $oauth_token_secret The OAuth secret
     *
     * @return null
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function setClientCredentials($oauth_token, $oauth_token_secret)
    {
        $this->oauth_token = trim($oauth_token);
        $this->oauth_token_secret = trim($oauth_token_secret);
        if ($this->consumer_key == '') {
            throw new OAuthException('OAuth key is not specified', OAuthException::ERROR_NO_KEY);
        } elseif ($this->consumer_secret == '') {
            throw new OAuthException('OAuth secret is not specified', OAuthException::ERROR_NO_SECRET);
        }
    }
    
    /**
     * A lot of methods are required for authentication, but this sums it down.
     * It will do HTTP redirects and uses the $_GET and $_SESSION globals, but makes authenticating easier.
     *
     * @param string $request_token_url The request-token URL
     * @param string $access_token_url  The access-token URL
     * @param string $authenticate_url  The authentication URL
     *                                  (when false, 2-legged OAuth will be used, otherwise3-legged OAuth will be used)
     * @param string $callback_url      The callback URL (optional)
     *
     * @return array An array with 2 keys: oauth_token, oauth_token_secret
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function authenticate($request_token_url, $access_token_url, $authenticate_url, $callback_url = null)
    {
        if ($authenticate_url === false) {
            return $this->authenticateTwoLegged($request_token_url, $access_token_url);
        } else {
            return $this->authenticateThreeLegged(
                $request_token_url,
                $access_token_url,
                $authenticate_url,
                $callback_url
            );
        }
    }
    
    /**
     * A lot of methods are required for authentication, but this sums it down for 3-legged OAuth.
     * It will do HTTP redirects and uses the $_GET and $_SESSION globals, but makes authenticating easier.
     *
     * @param string $request_token_url The request-token URL
     * @param string $access_token_url  The access-token URL
     * @param string $authenticate_url  The authentication URL
     * @param string $callback_url      The callback URL (optional)
     *
     * @return array An array with 2 keys: oauth_token, oauth_token_secret
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    private function authenticateThreeLegged(
        $request_token_url,
        $access_token_url,
        $authenticate_url,
        $callback_url = null
    ) {
        // Autodetects -and remembers- the redirect URI
        // Remembering is important; it can be validated on the second request, but when auto-detecting,
        // the ?code= is also taken into the URI
        if ($callback_url === null) {
            if (isset($_SESSION['oauth_callback_uri'][$this->consumer_key])) {
                $callback_url = $_SESSION['oauth_callback_uri'][$this->consumer_key];
            } else {
                $callback_url = self::getCurrentURL();
            }
            $_SESSION['oauth_callback_uri'][$this->consumer_key] = $callback_url;
        }
        
        // If we haven't recorded a step yet, set it to 0
        if (!isset($_SESSION['oauth_step'])) {
            $_SESSION['oauth_step'] = 0;
        }
        
        // First step, redirect to the OAuth service
        if ($_SESSION['oauth_step'] == 0) {
            $_SESSION['oauth_step'] = 1;
            // We'll need a request token
            $request_token = $this->getRequestToken($request_token_url, $authenticate_url, $callback_url);
            $_SESSION['oauth_request_token'] = $request_token['oauth_token'];
            $_SESSION['oauth_request_token_secret'] = $request_token['oauth_token_secret'];
            // The 302 addition is important; the URL is variable, so no permanently moved.
            header('Location: '.$request_token['authenticate_url'], 302);
            exit;
        } elseif ($_SESSION['oauth_step'] == 1) { // Second step, we'll request the credentials
            unset($_SESSION['oauth_step']);
            if (isset($_GET['oauth_verifier']) && isset($_GET['oauth_token'])) {
                // We are redirected from the OAuth server and can get an accesstoken
                $access_token = $this->getAccessToken(
                    $access_token_url,
                    $_SESSION['oauth_request_token_secret'],
                    $_GET['oauth_token'],
                    $_GET['oauth_verifier']
                );
                // Lets store the token and login
                $_SESSION['oauth_token'] = $access_token['oauth_token'];
                $_SESSION['oauth_token_secret'] = $access_token['oauth_token_secret'];
                $this->setClientCredentials($access_token['oauth_token'], $access_token['oauth_token_secret']);
                return $access_token;
            }
        }
        
        // If we'll come here, something went wrong
        unset($_SESSION['oauth_step']);
        foreach (array('error_description', 'error', 'oauth_problem', 'error_reason') as $error_field) {
            if (isset($_GET[$error_field])) {
                throw new OAuthException('The OAuth service reported a problem: '.$_GET[$error_field]);
            }
        }
        throw new OAuthException('The OAuth service reported an unknown problem');
    }
    
    /**
     * A lot of methods are required for authentication, but this sums it down for 2-legged OAuth.
     *
     * @param string $request_token_url The request-token URL
     * @param string $access_token_url  The access-token URL
     *
     * @return array An array with 2 keys: oauth_token, oauth_token_secret
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    private function authenticateTwoLegged($request_token_url, $access_token_url)
    {
        // Fetches a request token
        $request_token = $this->getRequestToken($request_token_url, null, false);
        // Requests an Access token
        $access_token = $this->getAccessToken(
            $access_token_url,
            $request_token['oauth_token_secret'],
            $request_token['oauth_token']
        );
        // Logs in
        $this->setClientCredentials($access_token['oauth_token'], $access_token['oauth_token_secret']);
        return $access_token;
    }
    
    /**
     * Gets an accesstoken
     *
     * @param string $access_token_url   The access-token URL
     * @param string $oauth_token_secret The OAuth secret
     * @param string $oauth_token        The OAuth token
     * @param string $oauth_verifier     (optional)
     *
     * @return array An array with 2 keys: oauth_token, oauth_token_secret
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function getAccessToken($access_token_url, $oauth_token_secret, $oauth_token, $oauth_verifier = null)
    {
        $data = array('oauth_token' => $oauth_token);
        if ($oauth_verifier !== null) {
            $data['oauth_verifier'] = $oauth_verifier;
        }
        // Stores the secret in memory temporarily
        $oauth_token_secret_tmp = $this->oauth_token_secret;
        $this->oauth_token_secret = $oauth_token_secret;
        // Executes the request
        $result = $this->OAuthRequest($access_token_url, 'POST', $data);
        // Restores the secret
        $this->oauth_token_secret = $oauth_token_secret_tmp;
        
        // Validates the response
        $params = $result->getObject();
        if (!isset($params->oauth_token) || !isset($params->oauth_token_secret)) {
            throw new OAuthException(
                'getAccessToken-call gave an invalid response',
                OAuthException::ERROR_INVALID_ACCESS_TOKEN_RESPONSE,
                null,
                $result
            );
        }
        
        // Sends back all data
        return array(
            'oauth_token' => $params->oauth_token,
            'oauth_token_secret' => $params->oauth_token_secret,
        );
    }
    
    /**
     * Gets a request token
     *
     * @param string $request_token_url The request-token URL
     * @param string $authenticate_url  The authentication URL (optional)
     * @param string $callback_url      The callback URL (optional; null autodetects current URL,
     *                                  false will leave the URL from the request)
     *
     * @return array An array with 3 keys: oauth_token, oauth_token_secret and authenticate_url
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function getRequestToken($request_token_url, $authenticate_url = null, $callback_url = null)
    {
        $data = array();
        // Default value for the callback URL is the current URL
        if ($callback_url === null) {
            $callback_url = self::getCurrentURL();
        }
        // If the callback URL is defined, we'll use this
        if ($callback_url) {
            $data['oauth_callback'] = $callback_url;
        }
        // Performs the actual request
        $result = $this->OAuthRequest($request_token_url, 'POST', $data);
        
        // Validates the response
        $params = $result->getObject();
        if (!isset($params->oauth_token) || !isset($params->oauth_token_secret)) {
            throw new OAuthException(
                'getRequestToken-call gave an invalid response',
                OAuthException::ERROR_INVALID_REQUEST_TOKEN_RESPONSE,
                null,
                $result
            );
        }
        if ($callback_url
            && (!isset($params->oauth_callback_confirmed) || $params->oauth_callback_confirmed != 'true')
        ) {
            throw new OAuthException(
                'getRequestToken didn\'t confirm the callback',
                OAuthException::ERROR_REQUEST_TOKEN_RESPONSE_NO_CALLBACK,
                null,
                $result
            );
        }
        
        // Generates the authentication URL
        if ($authenticate_url === null) {
            $authenticate_url = '';
        }
        $authenticate_url .= strpos($authenticate_url, '?') === false ? '?' : '&';
        $authenticate_url .= 'oauth_token='.$params->oauth_token;
        
        // Sends back all data
        return array(
            'oauth_token' => $params->oauth_token,
            'oauth_token_secret' => $params->oauth_token_secret,
            'authenticate_url' => $authenticate_url,
        );
    }
    
    /**
     * Executes a cURL request
     *
     * @param string $consumer_secret App secret
     * @param string $token_secret    Client secret (may be null, if we haven't got one)
     * @param string $url             The URL to be requested
     * @param string $request_method  So far it may be GET, POST
     * @param array  $data            POST or GET values
     *
     * @return string The OAuth signature
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    private static function OAuthSignRequest(
        $consumer_secret,
        $token_secret,
        $url,
        $request_method = 'GET',
        array $data = array()
    ) {
        // Do we have a signature method?
        if (!isset($data['oauth_signature_method'])) {
            throw new OAuthException('No signature method specified', ERROR_NO_SIGNATURE_METHOD);
        }
        // Token secret may be optional, depending on the state of the authentication
        if ($token_secret === null) {
            $token_secret = '';
        }
        // Data needs to be sorted
        ksort($data);
        
        // Building the base string and signing key
        $signature_base_string = strtoupper($request_method).'&'.rawurlencode($url).'&'
            .rawurlencode(self::arrayToRequestURI($data));
        $signing_key = rawurlencode($consumer_secret).'&'.rawurlencode($token_secret);
        
        // Applying the right signature method
        if (strtoupper($data['oauth_signature_method']) == 'PLAINTEXT') {
            return $signing_key;
        } elseif (strtoupper($data['oauth_signature_method']) == 'HMAC-SHA1') {
             return base64_encode(hash_hmac('sha1', $signature_base_string, $signing_key, true));
        } else {
            throw new OAuthException(
                'Signature method not supported: '.strtoupper($data['oauth_signature_method']),
                OAuthException::ERROR_SIGNATURE_METHOD_UNSUPPORTED
            );
        }
    }
    
    /**
     * Performs a signed OAuth webrequest
     *
     * @param string $url            The URL to be requested
     * @param string $request_method So far it may be GET, POST
     * @param array  $data           POST or GET values
     * @param array  $headers        Additional HTTP headers
     * @param string $payload        Additional payload to be posted
     *
     * @return OAuthResponse
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function OAuthRequest(
        $url,
        $request_method = 'GET',
        array $data = array(),
        array $headers = array(),
        $payload = null
    ) {
        // These fields are required for OAuth requests
        $oauth_data = array(
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => self::getUnique(),
            'oauth_signature_method' => $this->signature_method,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0',
        );
        // Adds other OAuth values from the user data
        foreach ($data as $key => $value) {
            if (substr($key, 0, 6) == 'oauth_') {
                $oauth_data[$key] = $value;
                unset($data[$key]);
            }
        }
        // Only adds the OAuth token if there is one
        if ($this->oauth_token && !isset($oauth_data['oauth_token'])) {
            $oauth_data['oauth_token'] = $this->oauth_token;
        }
        ksort($oauth_data);
        
        // Builds the Authorization-header
        if ($this->oauth_echo_enabled) {
            // With OAuth Echo-ing, two headers are required, and we verify against the service provider instead.
            $authorization = 'X-Verify-Credentials-Authorization: OAuth '
                .($this->oauth_realm?'realm="'.$this->oauth_realm.'"':'');
            $headers[] = 'X-Auth-Service-Provider: '.$this->serviceprovider_url;
            $oauth_data['oauth_signature'] = self::OAuthSignRequest(
                $this->consumer_secret,
                $this->oauth_token_secret,
                $this->serviceprovider_url,
                'GET',
                $oauth_data
            );
        } else {
            // With non-echo-ing requests, it's just an Auth-header. The signature will contain OAuth- and userdata.
            $authorization = 'Authorization: OAuth '.($this->oauth_realm?'realm="'.$this->oauth_realm.'"':'');
            $oauth_data['oauth_signature'] = self::OAuthSignRequest(
                $this->consumer_secret,
                $this->oauth_token_secret,
                $url,
                $request_method,
                array_merge($oauth_data, $data)
            );
        }
        // Adds all OAuth data to the authorization header
        foreach ($oauth_data as $key => $value) {
            $authorization .= ",".rawurlencode($key).'="'.rawurlencode($value).'"';
        }
        $authorization = str_replace('OAuth ,', 'OAuth ', $authorization); // Removes the first comma
        $headers[] = $authorization;
        
        // Executes the request
        $return = $this->curlRequest($url, $request_method, $data, $headers, $payload);
        // Validates the response
        if ($return->getHttpStatus() < 200 || $return->getHttpStatus() > 299) {
            throw new OAuthException(
                'Unexpected HTTP status #'.$return->getHttpStatus(),
                OAuthException::ERROR_HTTP + $return->getHttpStatus(),
                null,
                $return
            );
        }
        return $return;
    }
    
    /**
     * Enables an OAuth echo service
     *
     * @param string $serviceprovider_url The URL of the service provider
     *
     * @return null
     * @access public
     */
    public function enableOAuthEcho($serviceprovider_url)
    {
        $this->oauth_echo_enabled = true;
        $this->serviceprovider_url = $serviceprovider_url;
    }
    
    /**
     * Disables an OAuth echo service
     *
     * @return null
     * @access public
     */
    public function disableOAuthEcho()
    {
        $this->oauth_echo_enabled = false;
    }
}
