<?php
/**
 * OAuth support for PHP
 *
 * Minimal required PHP version is 5.6
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @copyright  2017 Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @version    HG: $Id$
 * @link       https://developers.ibizz.nl/packages/oauth
 */
namespace StudioIbizz\OAuth;

/**
 * This class contains the OAuth response data
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @link       https://developers.ibizz.nl/packages/oauth
 */
class OAuthResponse
{
    
    /**
     * All options as sent to cURL
     *
     * @access private
     * @var    array
     */
    private $curl_options;
    
    /**
     * All information from the cURL request
     *
     * @access private
     * @var    array
     */
    private $curl_info;
    
    /**
     * All HTTP headers
     *
     * @access private
     * @var    array
     */
    private $response_headers;
    
    /**
     * The actual data
     *
     * @access private
     * @var    string
     */
    private $response_data;
    
    /**
     * The data parsed as object
     *
     * @access private
     * @var    object
     */
    private $response_object;
    
    /**
     * Initializes a new OAuthResponse object
     *
     * @param array  $curl_options cURL options
     * @param array  $curl_info    cURL information
     * @param string $data         cURL response
     *
     * @return null
     * @access public
     */
    public function __construct(array $curl_options, array $curl_info, $data)
    {
        $this->curl_options = $curl_options;
        $this->curl_info = $curl_info;

        // Splits headers and reponse
        $this->response_headers = self::splitHeaders(substr($data, 0, $this->curl_info['header_size']));
        $this->response_data = substr($data, $this->curl_info['header_size']);

        // Converts the data to an object if possible
        switch ($this->getContentType()) {
            case 'application/json':
                $this->response_object = json_decode($this->response_data);
                break;
            case 'application/xml':
            case 'text/xml':
                $this->response_object = simplexml_load_string($this->response_data);
                break;
            default:
                // No recognizable content type sent back, we're going to auto-detect it
                $object = null;
                if (substr($this->response_data, 0, 1) == '{') {
                    $object = @json_decode($this->response_data);
                } elseif (substr($this->response_data, 0, 2) == '<?') {
                    $object = @simplexml_load_string($this->response_data);
                }
                $this->response_object = $object === null ? self::requestURIToObject($this->response_data) : $object;
        }
    }

    /**
     * Returns the response data
     *
     * @access public
     * @return string
     */
    public function __toString()
    {
        return $this->response_data;
    }
    
    /**
     * Returns the HTTP status code
     *
     * @return int
     * @access public
     */
    public function getHttpStatus()
    {
        return (int)$this->curl_info['http_code'];
    }
    
    /**
     * Returns the content type
     *
     * @return string
     * @access public
     */
    public function getContentType()
    {
        // Sometimes a charset is also added
        $parts = explode(';', $this->curl_info['content_type'], 2);
        return $parts[0];
    }
    
    /**
     * Returns the response data as object
     *
     * @access public
     * @return \stdClass
     */
    public function getObject()
    {
        return $this->response_object;
    }
    
    /**
     * Returns an array of all the response headers sent back by the OAuth service
     *
     * @return array
     * @access public
     */
    public function getResponseHeaders()
    {
        return $this->response_headers;
    }
    
    /**
     * Returns an array with cURL information
     *
     * @return array
     * @access public
     */
    public function getCurlInfo()
    {
        return $this->curl_info;
    }
    
    /**
     * Returns an array with all cURL options set for the request that resulted in this response
     *
     * @return array
     * @access public
     */
    public function getCurlOptions()
    {
        return $this->curl_options;
    }
    
    /**
     * Converts a Request URI to an object
     *
     * @param string $request_uri Example: id=34&action=edit
     *
     * @return \stdClass
     * @access private
     */
    private static function requestURIToObject($request_uri)
    {
        $return = new \stdClass();
        
        // If a full URL is specified, only use the query part
        // Otherwise just assume $request_uri only contains a query part
        $url_parts = parse_url($request_uri);
        if (isset($url_parts['query'])) {
            $querystring = $url_parts['query'];
        } else {
            $querystring = $request_uri;
        }
        // Empty query string results in an empty array
        if (!$querystring) {
            return $return;
        }
        
        // If the querystring starts with a ? it needs to be removed
        $querystring = ltrim($querystring, '?');
        
        // Explodes all variables
        $variables = explode('&', $querystring);
        foreach ($variables as $iterator => $variable) {
            $parts = explode('=', $variable, 2);
            if (count($parts) == 1) {
                $key = $parts[0];
                $value = null;
            } else {
                list($key, $value) = $parts;
            }
            if (!$key) {
                $key = $iterator;
            }
            $return->$key = rawurldecode($value);
        }
        
        return $return;
    }
    
    /**
     * Converts HTTP headers from string to array
     *
     * @param string $headers The headers as string
     *
     * @return array
     * @access private
     */
    private static function splitHeaders($headers)
    {
        $lines = explode("\n", $headers);
        foreach ($lines as $line_number => $line) {
            if ((substr_count($line, ':') < 1 || preg_match('/^[\s]{1}/', $line))
                && $line_number > 0
                && isset($lines[$line_number - 1])
            ) {
                // Validates if this is a valid header
                // When there's no colon or when it starts with whitespace, and when it's not the first line
                $lines[$line_number - 1] .= ' '.trim($line);
                unset($lines[$line_number]);
            } elseif (trim($line == '')) {
                // Also, skip empty lines
                unset($lines[$line_number]);
            } else {
                // Remove additional ending whitespace
                $lines[$line_number] = rtrim($line);
            }
        }
        
        return array_values($lines);
    }
}
