<?php
/**
 * OAuth support for PHP
 *
 * Minimal required PHP version is 5.6
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @copyright  2017 Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @version    HG: $Id$
 * @link       https://developers.ibizz.nl/packages/oauth
 */
namespace StudioIbizz\OAuth;

/**
 * This class contains the OAuth exceptions
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @link       https://developers.ibizz.nl/packages/oauth
 */
class OAuthException extends \Exception
{
    /**
     * Values above ERROR_HTTP: HTTP error, subtract ERROR_HTTP to get the HTTP-code
     *
     * @access public
     */
    const ERROR_HTTP = 1000;
    
    /**
     * Values between 500 and 999 are cURL errors
     *
     * See http://curl.haxx.se/libcurl/c/libcurl-errors.html for those error codes
     *
     * @access public
     */
    const ERROR_CURL = 500;
    
    /**
     * No signature method is specified
     *
     * @access public
     */
    const ERROR_NO_SIGNATURE_METHOD = 1;
    
    /**
     * The specified signature method is not supported
     *
     * @access public
     */
    const ERROR_SIGNATURE_METHOD_UNSUPPORTED = 2;
    
    /**
     * The method getAccessToken-call gave an invalid response
     *
     * @access public
     */
    const ERROR_INVALID_ACCESS_TOKEN_RESPONSE = 3;
    
    /**
     * The method getRequestToken-call gave an invalid response
     *
     * @access public
     */
    const ERROR_INVALID_REQUEST_TOKEN_RESPONSE = 4;
    
    /**
     * The method getRequestToken didn't confirm the callback
     *
     * @access public
     */
    const ERROR_REQUEST_TOKEN_RESPONSE_NO_CALLBACK = 5;
    
    /**
     * Consumer/OAuth key is not specified
     *
     * @access public
     */
    const ERROR_NO_KEY = 6;
    
    /**
     * Consumer/OAuth secret is not specified
     *
     * @access public
     */
    const ERROR_NO_SECRET = 7;
    
    /**
     * OAuth token is not specified
     *
     * @access public
     */
    const ERROR_NO_TOKEN = 8;
    
    /**
     * OAuth token type is not specified
     *
     * @access public
     */
    const ERROR_NO_TOKEN_TYPE = 9;
    
    /**
     * The state is modified during the request
     *
     * @access public
     */
    const ERROR_STATE_DOESNT_MATCH = 10;
    
    /**
     * Payload not possible with this kind of request
     *
     * @access public
     */
    const ERROR_WRONG_REQUEST_FOR_PAYLOAD = 11;
    
    /**
     * The specified parameter $token_transfer is not valid.
     *
     * @access public
     */
    const ERROR_INVALID_TOKEN_TRANSFER_TYPE = 12;
    
    /**
     * Payload-parameter not used or empty. Payload is required when executing a PUT request.
     *
     * @access public
     */
    const ERROR_PUT_REQUIRES_PAYLOAD = 13;
    
    /**
     * Stores additional cURL data
     *
     * @access private
     * @var    OAuthResponse
     */
    private $OAuthResponse = null;
    
    /**
     * Throws an exception
     *
     * @param string        $message       The Exception message to throw.
     * @param int           $code          The Exception code.
     * @param Exception     $previous      The previous exception used for the exception chaining.
     * @param OAuthResponse $OAuthResponse The OAuth response
     *
     * @return null
     * @access public
     */
    public function __construct(
        $message = "",
        $code = 0,
        Exception $previous = null,
        OAuthResponse $OAuthResponse = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->OAuthResponse = $OAuthResponse;
    }
    
    /**
     * Sends back the OAuth response
     *
     * @access public
     * @return OAuthResponse
     */
    public function getOAuthResponse()
    {
        return $this->OAuthResponse;
    }
}
