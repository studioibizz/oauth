<?php
/**
 * OAuth support for PHP
 *
 * Minimal required PHP version is 5.6
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @copyright  2017 Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @version    HG: $Id$
 * @link       https://developers.ibizz.nl/packages/oauth
 */
namespace StudioIbizz\OAuth;

/**
 * This class contains the OAuth v2 functionality
 *
 * @category   Classes
 * @package    StudioIbizz\OAuth
 * @subpackage OAuth
 * @author     Stefan Thoolen <stefan@ibizz.nl>
 * @license    http://creativecommons.org/licenses/by-sa/3.0/ Creative Commons Attribution-ShareAlike 3.0 Unported
 * @link       https://developers.ibizz.nl/packages/oauth
 */
class OAuth2 extends OAuth
{
    /**
     * Client ID of the OAuth provider
     *
     * @access private
     * @var    string
     */
    private $client_id;
    /**
     * Client secret of the OAuth provider
     *
     * @access private
     * @var    string
     */
    private $client_secret;
    /**
     * Access token of the OAuth customer
     *
     * @access private
     * @var    string
     */
    private $access_token = null;
    /**
     * Token type of the OAuth customer
     *
     * @access private
     * @var    string
     */
    private $token_type = 'Bearer';
    /**
     * The scope for the OAuth provider
     *
     * @access private
     * @var    array
     */
    private $scope;
    
    /**
     * Method of the token transfer (TOKEN_TRANSFER_HEADER or TOKEN_TRANSFER_REQUEST_URI)
     *
     * @access private
     * @var    integer
     */
    private $token_transfer;

    /**
     * The OAuth version of this service (always 2)
     *
     * @access protected
     * @var    integer
     */
    protected static $oauth_version = 2;
    
    /**
     * The token is sent as header (default value)
     *
     * @access public
     */
    const TOKEN_TRANSFER_HEADER = 1;
    
    /**
     * The token is sent in the request URI
     *
     * @access public
     */
    const TOKEN_TRANSFER_REQUEST_URI = 2;
    
    /**
     * Creates a new OAuth object
     *
     * @param string  $client_id      Client ID of the OAuth provider
     * @param string  $client_secret  Client secret of the OAuth provider
     * @param array   $scope          The scope for the OAuth provider
     * @param integer $token_transfer Method of the token transfer (TOKEN_TRANSFER_HEADER or TOKEN_TRANSFER_REQUEST_URI)
     *
     * @return null
     * @access public
     */
    public function __construct(
        $client_id,
        $client_secret,
        array $scope = array(),
        $token_transfer = self::TOKEN_TRANSFER_HEADER
    ) {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->scope = $scope;
        $this->token_transfer = $token_transfer;
        if (!in_array($this->token_transfer, array(self::TOKEN_TRANSFER_HEADER, self::TOKEN_TRANSFER_REQUEST_URI))) {
            throw new OAuthException(
                'The specified parameter $token_transfer is not valid. Expected values: '
                .__CLASS__.'::TOKEN_TRANSFER_HEADER or '.__CLASS__.'::TOKEN_TRANSFER_REQUEST_URI',
                OAuthException::ERROR_INVALID_TOKEN_TRANSFER_TYPE
            );
        }
        // By default, we'll verify the SSL certificate (if using HTTPS)
        $this->setCurlOption(CURLOPT_SSL_VERIFYHOST, 2);
        $this->setCurlOption(CURLOPT_SSL_VERIFYPEER, true);
    }
    
    /**
     * Returns the Authorize URL including all required parameters
     *
     * @param string $authorize_url The authorize URL
     * @param string $redirect_uri  The callback URL
     *
     * @return string The $authorize_url with added parameters
     * @access public
     */
    public function getAuthorizeURL($authorize_url = null, $redirect_uri = null)
    {
        // Generates a unique state
        $state = self::getUnique();
        
        // Compiles all parameters
        $data = array(
            'client_id' => $this->client_id,
            'redirect_uri' => isset($redirect_uri) ? $redirect_uri : self::getCurrentURL(),
            'state' => $state,
            'response_type' => 'code',
        );
        
        // If there is a scope, add it
        if (count($this->scope)) {
            $data['scope'] = implode(',', $this->scope);
        }
        
        // Compiles the URL
        $authorize_url = self::arrayToRequestURI($data, $authorize_url);
        
        // Sends back the URL and state
        return array(
            'authorize_url' => $authorize_url,
            'state' => $state,
        );
    }
    
    /**
     * A lot of methods are required for authentication, but this sums it down.
     * It will do HTTP redirects and uses the $_GET and $_SESSION globals, but makes authenticating easier.
     *
     * @param string $authorize_url    The authorize URL
     * @param string $access_token_url The access token URL
     * @param string $redirect_uri     The callback URL
     *
     * @return array An array with three keys: 'access_token', 'token_type' and 'scope'
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function authenticate($authorize_url, $access_token_url, $redirect_uri = null)
    {
        // Autodetects -and remembers- the redirect URI
        // Remembering is important; it can be validated on the second request, but when auto-detecting,
        // the ?code= is also taken into the URI
        if ($redirect_uri === null) {
            if (isset($_SESSION['oauth_redirect_uri'][$this->client_id])) {
                $redirect_uri = $_SESSION['oauth_redirect_uri'][$this->client_id];
            } else {
                $redirect_uri = self::getCurrentURL();
            }
            $_SESSION['oauth_redirect_uri'][$this->client_id] = $redirect_uri;
        }
        
        // If we haven't recorded a step yet, set it to 0
        if (!isset($_SESSION['oauth_step'])) {
            $_SESSION['oauth_step'] = 0;
        }
        
        // First step, redirect to the OAuth service
        if ($_SESSION['oauth_step'] == 0) {
            $_SESSION['oauth_step'] = 1;
            // We'll need an authorisation URL
            $authorize = $this->getAuthorizeURL($authorize_url, $redirect_uri);
            $_SESSION['oauth_state'] = $authorize['state'];
            // The 302 addition is important; the URL is variable, so no permanently moved.
            header('Location: '.$authorize['authorize_url'], 302);
            exit;
        } elseif ($_SESSION['oauth_step'] == 1) { // Second step, we'll request the credentials
            unset($_SESSION['oauth_step']);
            if (isset($_GET['code']) && isset($_GET['state'])) {
                if ($_GET['state'] != $_SESSION['oauth_state']) {
                    throw new OAuthException('The state is modified during the request', ERROR_STATE_DOESNT_MATCH);
                }
                $return = $this->getAccessToken($_GET['code'], $access_token_url, $redirect_uri);
                $this->setClientCredentials($return['access_token'], $return['token_type']);
                return $return;
            }
        }
        
        // If we'll come here, something went wrong
        unset($_SESSION['oauth_step']);
        foreach (array('error_description', 'error', 'oauth_problem', 'error_reason') as $error_field) {
            if (isset($_GET[$error_field])) {
                throw new OAuthException('The OAuth service reported a problem: '.$_GET[$error_field]);
            }
        }
        throw new OAuthException('The OAuth service reported an unknown problem');
    }
    
    /**
     * Gets an accesstoken
     *
     * @param string $oauth_code       Code returned by the redirect from getAuthorizeURL
     * @param string $access_token_url The access token URL
     * @param string $redirect_uri     The callback URL
     *
     * @return array An array with four keys: 'access_token', 'refresh_token', 'token_type' and 'scope'
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function getAccessToken($oauth_code, $access_token_url, $redirect_uri = null)
    {
        $data = array(
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'code' => $oauth_code,
            'redirect_uri' => isset($redirect_uri) ? $redirect_uri : self::getCurrentURL(),
            'grant_type' => 'authorization_code',
        );
        
        return $this->requestAccessToken($data, $access_token_url);
    }
    
    /**
     * Refreshes the access token
     *
     * @param string $refresh_token    The refresh token
     * @param string $access_token_url The access token URL
     *
     * @return array An array with four keys: 'access_token', 'refresh_token', 'token_type' and 'scope'
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     */
    public function refreshAccessToken($refresh_token, $access_token_url)
    {
        $data = array(
            'grant_type' => 'refresh_token',
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'refresh_token' => $refresh_token,
        );
        
        return $this->requestAccessToken($data, $access_token_url);
    }
    
    /**
     * Requests an access token
     *
     * @param array  $data             All request data
     * @param string $access_token_url The access token URL
     *
     * @return array An array with four keys: 'access_token', 'refresh_token', 'token_type' and 'scope'
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     */
    public function requestAccessToken($data, $access_token_url)
    {
        $result = $this->curlRequest($access_token_url, 'POST', $data);
        
        // Validates the response
        $params = $result->getObject();
        if (!isset($params->access_token) || !isset($params->access_token)) {
            throw new OAuthException(
                'getAccessToken-call gave an invalid response',
                OAuthException::ERROR_INVALID_ACCESS_TOKEN_RESPONSE,
                null,
                $result
            );
        }
        // Fetches the scope
        if (property_exists($params, 'scope')) {
            $scope = preg_split('/[,\s]{1,}/', $params->scope);
        } else {
            $scope = array();
        }
        
        // Returns all properties and the token type and scope in the correct format
        $return = array(
            'access_token' => $params->access_token,
            'token_type' => property_exists($params, 'token_type') ? $params->token_type : 'Bearer',
            'refresh_token' => property_exists($params, 'refresh_token') ? $params->refresh_token : null,
            'scope' => $scope,
        );
        return array_merge(get_object_vars($params), $return);
    }
    
    /**
     * Sets the clients credentials
     *
     * @param string $access_token The OAuth access token
     * @param string $token_type   The type of the token, default: "Bearer"
     *
     * @return null
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function setClientCredentials($access_token, $token_type = 'Bearer')
    {
        $this->access_token = trim($access_token);
        $this->token_type = trim($token_type);
        if ($this->access_token == '') {
            throw new OAuthException('OAuth token is not specified', OAuthException::ERROR_NO_TOKEN);
        }
        if ($this->token_type == '') {
            throw new OAuthException('OAuth token type is not specified', OAuthException::ERROR_NO_TOKEN_TYPE);
        }
    }
    
    /**
     * Performs a signed OAuth webrequest
     *
     * @param string $url            The URL to be requested
     * @param string $request_method So far it may be GET, POST
     * @param array  $data           POST or GET values
     * @param array  $headers        Additional HTTP headers
     * @param string $payload        Additional payload to be posted
     *
     * @return OAuthResponse
     * @throws OAuthException If something went wrong, Exception->getCode() will be a OAuthException::ERROR_ constant.
     * @access public
     */
    public function OAuthRequest(
        $url,
        $request_method = 'GET',
        array $data = array(),
        array $headers = array(),
        $payload = null
    ) {
        if ($this->token_transfer == self::TOKEN_TRANSFER_HEADER) {
            $headers[] = 'Authorization: '.$this->token_type.' '.$this->access_token;
        } else {
            $data['access_token'] = $this->access_token;
        }
        $return = $this->curlRequest($url, $request_method, $data, $headers, $payload);
        // Validates the response
        if ($return->getHttpStatus() < 200 || $return->getHttpStatus() > 299) {
            throw new OAuthException(
                'Unexpected HTTP status #'.$return->getHttpStatus(),
                OAuthException::ERROR_HTTP + $return->getHttpStatus(),
                null,
                $return
            );
        }
        return $return;
    }
}
