<?php
use StudioIbizz\OAuth as OAuth;

// For debugging purposes
ini_set('display_errors', true);
error_reporting(E_ALL);

// The OAuth->authenticate() method requires sessions
session_start();

// Loads the configuration and OAuth classes
require '../configs/twitpic.config.php';
require '../../src/OAuthException.php';
require '../../src/OAuthResponse.php';
require '../../src/OAuth.php';
require '../../src/OAuth1.php';

echo '<plaintext>';
try {
    // Authenticates against the OAuth service
    $OAuth = new OAuth\OAuth1($config['consumer_key'], $config['consumer_secret']);
    $AuthenticateResult = $OAuth->authenticate($config['request_token_url'], $config['access_token_url'], $config['authenticate_url']);

    // Uploading oauth.jpg
    $OAuth->enableOAuthEcho($config['serviceprovider_url']);
    $UploadResult = $OAuth->OAuthRequest(
        'http://api.twitpic.com/2/upload.json',
        'POST',
        array(
        'key' => $config['api_key'],
        'message' => 'Test for OAuth Library',
        'media' => '@'.realpath('oauth.jpg'),
        )
    )->getObject();
    
    // Outputs all data
    echo 'Welcome, '.$UploadResult->user.'!'.PHP_EOL;
    echo 'Your uploaded picture can be found at '.$UploadResult->url.PHP_EOL;
    echo str_repeat('-', 70).PHP_EOL;
    echo '$AuthenticateResult = '.print_r($AuthenticateResult, true);
    echo str_repeat('-', 70).PHP_EOL;
    echo '$UploadResult = '.print_r($UploadResult, true);
    echo str_repeat('-', 70).PHP_EOL;
} catch (OAuth\OAuthException $exception) {
    echo 'Error #'.$exception->getCode().': '.$exception->getMessage().PHP_EOL;
    print_r($exception->getOAuthResponse());
}
