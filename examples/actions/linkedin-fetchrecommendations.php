<?php
use StudioIbizz\OAuth as OAuth;

// For debugging purposes
ini_set('display_errors', true);
error_reporting(E_ALL);

// The OAuth->authenticate() method requires sessions
session_start();

// Loads the configuration and OAuth classes
require '../configs/linkedin.config.php';
require '../../src/OAuthException.php';
require '../../src/OAuthResponse.php';
require '../../src/OAuth.php';
require '../../src/OAuth1.php';

try {
    // Authenticates against the OAuth service
    $OAuth = new OAuth\OAuth1($config['consumer_key'], $config['consumer_secret']);
    $AuthenticateResult = $OAuth->authenticate($config['request_token_url'], $config['access_token_url'], $config['authenticate_url']);

    // Requests the user data
    $RecommendationsResult = $OAuth->OAuthRequest('http://api.linkedin.com/v1/people/~/recommendations-received')->getObject();
    
    foreach ($RecommendationsResult->recommendation as $Recommendation) {
        // Fetches the user that placed the recommendation
        $usericon_url = (string)$OAuth->OAuthRequest("http://api.linkedin.com/v1/people/".$Recommendation->recommender->id."/picture-url")->getObject();
        $profile_url = 'https://www.linkedin.com/x/profile/'.$config['consumer_key'].'/'.$Recommendation->recommender->id;
        echo '<p>';
        echo '<img align="left" src="'.$usericon_url.'">';
        echo htmlspecialchars($Recommendation->{'recommendation-text'}).'<br>';
        echo '<em><a href="'.$profile_url.'" target="_blank">'.htmlspecialchars($Recommendation->recommender->{'first-name'}.' '.$Recommendation->recommender->{'last-name'}).'</a></em>';
        echo '</p><hr style="clear: both">';
    }

    // Outputs all data
    echo '<plaintext>';
    echo '$AuthenticateResult = '.print_r($AuthenticateResult, true);
    echo str_repeat('-', 70).PHP_EOL;
    echo '$RecommendationsResult = '.print_r($RecommendationsResult, true);
} catch (OAuth\OAuthException $exception) {
    echo '<plaintext>';
    echo 'Error #'.$exception->getCode().': '.$exception->getMessage().PHP_EOL;
    print_r($exception->getOAuthResponse());
}
