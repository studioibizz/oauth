<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Salesforce application
$config['client_id']         = '';
$config['client_secret']     = '';

// We'll request access to these scopes
$config['scope']             = array();

// These URLs are from the Salesforce OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://test.salesforce.com/services/oauth2/authorize';
$config['access_token_url']  = 'https://test.salesforce.com/services/oauth2/token';
