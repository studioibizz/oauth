<?php
// Twitpic uses Twitter as OAuth Echo provider
// So for using Twitpic, you'll need to register a Twitter app for authentication and Twitpic app for posting

// Let's inherit the Twitter config
require dirname(__FILE__).'/twitter.config.php';

// Service provider URL; this is the service provider for Twitter
$config['serviceprovider_url'] = 'https://api.twitter.com/1.1/account/verify_credentials.json';
// Twitpic API Key; you can get this from Twitpic
$config['api_key']             = '';
