<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Github application
$config['client_id']         = '';
$config['client_secret']     = '';

// We'll request access to these scopes
$config['scope']             = array('user');

// These URLs are from the Github OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://github.com/login/oauth/authorize';
$config['access_token_url']  = 'https://github.com/login/oauth/access_token';
