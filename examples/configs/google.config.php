<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Google application
$config['client_id']         = '';
$config['client_secret']     = '';

// We'll request access to these scopes
$config['scope']             = array('profile');

// These URLs are from the Google OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://accounts.google.com/o/oauth2/auth';
$config['access_token_url']  = 'https://accounts.google.com/o/oauth2/token';
