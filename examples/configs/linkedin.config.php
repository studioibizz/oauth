<?php
$config = array();

// The OAuth app key and secret can be retrieved by registring a new Linkedin application
$config['consumer_key']      = '';
$config['consumer_secret']   = '';

// These URLs are from the Linkedin OAuth documentation and shall probably not change
$config['request_token_url'] = 'https://api.linkedin.com/uas/oauth/requestToken';
$config['access_token_url']  = 'https://api.linkedin.com/uas/oauth/accessToken';
$config['authenticate_url']  = 'https://www.linkedin.com/uas/oauth/authenticate';
