<?php
$config = array();

// The OAuth app key and secret can be retrieved by registring a new Twitter application
$config['consumer_key']      = '';
$config['consumer_secret']   = '';

// These URLs are from the Twitter OAuth documentation and shall probably not change
$config['request_token_url'] = 'https://api.twitter.com/oauth/request_token';
$config['access_token_url']  = 'https://api.twitter.com/oauth/access_token';
$config['authenticate_url']  = 'https://api.twitter.com/oauth/authorize';
