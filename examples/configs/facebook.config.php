<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Facebook application
$config['client_id']         = '';
$config['client_secret']     = '';

// Here you can add scope items, but for requesting basic information, no scope is needed
$config['scope']             = array();

// These URLs are from the Facebook OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://www.facebook.com/dialog/oauth';
$config['access_token_url']  = 'https://graph.facebook.com/oauth/access_token';
