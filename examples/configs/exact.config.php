<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Google application
$config['client_id']         = '';
$config['client_secret']     = '';

// These URLs are from the Google OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://start.exactonline.nl/api/oauth2/auth';
$config['access_token_url']  = 'https://start.exactonline.nl/api/oauth2/token';
