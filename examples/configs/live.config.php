<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Microsoft Live application
$config['client_id']         = '';
$config['client_secret']     = '';

// We'll request access to these scopes
$config['scope']             = array('wl.basic', 'wl.signin');

// These URLs are from the Microsoft Live OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://login.live.com/oauth20_authorize.srf';
$config['access_token_url']  = 'https://login.live.com/oauth20_token.srf';
