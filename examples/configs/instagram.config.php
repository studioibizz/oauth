<?php
$config = array();

// The client ID and secret can be retrieved by registring a new Instagram application
$config['client_id']         = '';
$config['client_secret']     = '';

// We'll request access to these scopes
$config['scope']             = array();

// These URLs are from the Instagram OAuth documentation and shall probably not change
$config['authorize_url']     = 'https://api.instagram.com/oauth/authorize';
$config['access_token_url']  = 'https://api.instagram.com/oauth/access_token';
