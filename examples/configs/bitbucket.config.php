<?php
$config = array();

// The OAuth app key and secret can be retrieved by registring a new Bitbucket application
$config['consumer_key']      = '';
$config['consumer_secret']   = '';

// These URLs are from the Bitbucket OAuth documentation and shall probably not change
$config['request_token_url'] = 'https://bitbucket.org/api/1.0/oauth/request_token';
$config['access_token_url']  = 'https://bitbucket.org/api/1.0/oauth/access_token';
$config['authenticate_url']  = 'https://bitbucket.org/api/1.0/oauth/authenticate';
