<?php
use StudioIbizz\OAuth as OAuth;

// For debugging purposes
ini_set('display_errors', true);
error_reporting(E_ALL);

// The OAuth->authenticate() method requires sessions
session_start();

// Loads the configuration and OAuth classes
require '../configs/exact.config.php';
require '../../src/OAuthException.php';
require '../../src/OAuthResponse.php';
require '../../src/OAuth.php';
require '../../src/OAuth2.php';

echo '<plaintext>';
try {
    // Authenticates against the OAuth service
    $OAuth = new OAuth\OAuth2($config['client_id'], $config['client_secret'], array(), OAuth\OAuth2::TOKEN_TRANSFER_REQUEST_URI);
    $AuthenticateResult = $OAuth->authenticate($config['authorize_url'], $config['access_token_url']);

    // Requests the user data
    $UserResult = $OAuth->OAuthRequest('https://start.exactonline.nl/api/v1/current/Me')->getObject()->entry->content->children('m', true)->children('d', true);
    
    // Outputs all data
    echo 'Welcome, '.$UserResult->FullName.'!'.PHP_EOL;
    echo str_repeat('-', 70).PHP_EOL;
    echo '$AuthenticateResult = '.print_r($AuthenticateResult, true);
    echo str_repeat('-', 70).PHP_EOL;
    echo '$UserResult = '.print_r($UserResult, true);
} catch (OAuth\OAuthException $exception) {
    echo 'Error #'.$exception->getCode().': '.$exception->getMessage().PHP_EOL;
    print_r($exception->getOAuthResponse());
}
